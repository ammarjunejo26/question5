package com.ecom.entities;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class Product implements Serializable{

	private static final long serialVersionUID = 1L;
	
	public Product() {}


	public Product(long id, String name, String price) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
	}

	@Id
	@GenericGenerator(name="generator", strategy="increment")
	@GeneratedValue(generator="generator")
	private long id;
	
	private String name;
//	private BigDecimal price;
	private String price;

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}



	


	
	
}
