package com.ecom.config;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class HibernateUtil {

	private static StandardServiceRegistry registry;
	private static SessionFactory sessionFactory;

	public static SessionFactory getSessionFactory() {
		if (sessionFactory == null) {
			try {
//									    	  Configuration configuration = new Configuration();
//											    configuration.configure();
				//							    ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
				//							                .applySettings(configuration.getProperties()).build();
				//							    sessionFactory = configuration.buildSessionFactory(serviceRegistry);

				registry = new StandardServiceRegistryBuilder().configure().build();

				// Create MetadataSources
				MetadataSources sources = new MetadataSources(registry);

				// Create Metadata
				Metadata metadata = sources.getMetadataBuilder().build();

				// Create SessionFactory
				sessionFactory = metadata.getSessionFactoryBuilder().build();
				
//				StandardServiceRegistry standardRegistry = 
//					       new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
//						Metadata metaData = 
//					        new MetadataSources(standardRegistry).getMetadataBuilder().build();
//						sessionFactory = metaData.getSessionFactoryBuilder().build();


			} catch (Exception e) {
				e.printStackTrace();
				if (registry != null) {
					StandardServiceRegistryBuilder.destroy(registry);
				}
			}
		}
		return sessionFactory;
	}

	public static void shutdown() {
		if (registry != null) {
			StandardServiceRegistryBuilder.destroy(registry);
		}
	}
}
