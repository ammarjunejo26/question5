package com.ecom.services;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.ShortBufferException;
import javax.enterprise.context.RequestScoped;

import com.ecom.entities.Product;
import com.ecom.repositories.IProductRepository;
import com.ecom.repositories.ProductRepositoryImpl;
import com.ecom.utils.Aes256;

@RequestScoped
public class ProductServiceImpl implements IProductService{

	public  List<Product>  getProducts() throws InvalidParameterSpecException, InvalidKeySpecException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, ShortBufferException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException, UnsupportedEncodingException, InvalidAlgorithmParameterException{
		IProductRepository productRepository =  new ProductRepositoryImpl();

		List<Product> productList = new ArrayList<Product>();
		productList = productRepository.findAll();

		for(Product product : productList){
			String price= null;
			price = Aes256.decrypt(product.getPrice());
			product.setPrice(price);
		}
		return productList;
	}

	public Product createProduct(Product product) throws InvalidKeySpecException ,NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, ShortBufferException, NoSuchProviderException, InvalidParameterSpecException{
		IProductRepository productRepository =  new ProductRepositoryImpl();

		String price = Aes256.encrypt(product.getPrice().toString());
		product.setPrice(price);
		product = productRepository.create(product);
		return product;
	}
	public void deleteProduct(Product product){
		IProductRepository productRepository =  new ProductRepositoryImpl();
		productRepository.delete(product);
	}
	
	public void editProduct(Product product) throws InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, ShortBufferException, NoSuchAlgorithmException, UnsupportedEncodingException, InvalidKeySpecException, InvalidParameterSpecException{
		IProductRepository productRepository =  new ProductRepositoryImpl();
		
		String price = Aes256.encrypt(product.getPrice().toString());
		product.setPrice(price);
		
		productRepository.update(product);
	}

}
