package com.ecom.services;

import java.security.NoSuchAlgorithmException;

import com.ecom.entities.User;

public interface IUserService {

	public User login(String username, String password) throws NoSuchAlgorithmException;
	
}
