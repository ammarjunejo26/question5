package com.ecom.services;


import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.enterprise.context.RequestScoped;
import javax.xml.bind.DatatypeConverter;

import com.ecom.entities.User;
import com.ecom.repositories.IUserRepository;
import com.ecom.repositories.UserRepositoryImpl;

@RequestScoped
public class UserServiceImpl implements IUserService{

	public User login(String username, String password) throws NoSuchAlgorithmException{
		//later make @inject
		IUserRepository userRepository = new UserRepositoryImpl();
        System.out.println(username + " <== service imp: ==> "  + password);

		//hash password here
		byte [] input = password.getBytes();
		MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
		messageDigest.update(input);
		byte[] digestedBytes = messageDigest.digest();
		String hashValue = DatatypeConverter.printHexBinary(digestedBytes).toLowerCase();
						
		User user = userRepository.validateUser(username, hashValue);
		
		if(user!=null){
			return user;
		}
		else{
			return null;
		}
		
	}
}
