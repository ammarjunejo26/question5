package com.ecom.services;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.ShortBufferException;

import com.ecom.entities.Product;

public interface IProductService {

	public List<Product> getProducts() throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, ShortBufferException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException, InvalidKeySpecException, InvalidParameterSpecException, UnsupportedEncodingException, InvalidAlgorithmParameterException;
	
	public Product createProduct(Product product) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, ShortBufferException, NoSuchProviderException, InvalidKeySpecException, InvalidParameterSpecException;
	
	public void deleteProduct(Product product);

	public void editProduct(Product product) throws InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, ShortBufferException, NoSuchAlgorithmException, UnsupportedEncodingException, InvalidKeySpecException, InvalidParameterSpecException;

}
