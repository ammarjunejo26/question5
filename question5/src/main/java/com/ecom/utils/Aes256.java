package com.ecom.utils;

import java.io.UnsupportedEncodingException;
import java.security.AlgorithmParameters;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.ShortBufferException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

public class Aes256 {

	private static int keySize = 256;
	private static int pswdIterations = 1000;
	private static String password = "test";

	public static String encrypt(final String price) throws NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, ShortBufferException, NoSuchAlgorithmException, UnsupportedEncodingException, InvalidKeySpecException, InvalidParameterSpecException {

		String salt;
		int keySize = 256;
		byte[] ivBytes;

		salt = generateSalt();
		byte[] saltBytes = salt.getBytes("UTF-8");

		SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		PBEKeySpec spec = new PBEKeySpec(password.toCharArray(),saltBytes,pswdIterations,keySize);

		SecretKey secretKey = factory.generateSecret(spec);
		SecretKeySpec secret = new SecretKeySpec(secretKey.getEncoded(), "AES");

		//encrypt the message
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, secret);
		AlgorithmParameters params = cipher.getParameters();
		ivBytes = params.getParameterSpec(IvParameterSpec.class).getIV();
		byte[] encryptedTextBytes = cipher.doFinal(price.getBytes("UTF-8"));

		String encodedText = Base64.encodeBase64String(encryptedTextBytes);
		String encodedIV = Base64.encodeBase64String(ivBytes);
		String encodedSalt = Base64.encodeBase64String(saltBytes);
		String encodedPackage = encodedSalt + "]" + encodedIV + "]" + encodedText;
		return encodedPackage;
	}

	public static String decrypt(final String encryptedPrice) throws  NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, ShortBufferException, NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, InvalidAlgorithmParameterException   {

		System.out.println(encryptedPrice);
		byte[] ivBytes;
		String[] fields = encryptedPrice.split("]");
		byte[] saltBytes = Base64.decodeBase64(fields[0]);
		ivBytes = Base64.decodeBase64(fields[1]);
		byte[] encryptedTextBytes = Base64.decodeBase64(fields[2]);

		// generate a key
		SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		PBEKeySpec spec = new PBEKeySpec(password.toCharArray(),saltBytes,pswdIterations,keySize);

		SecretKey secretKey = factory.generateSecret(spec);
		SecretKeySpec secret = new SecretKeySpec(secretKey.getEncoded(), "AES");

		// Decrypt msg here
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, secret, new IvParameterSpec(ivBytes));

		byte[] decryptedTextBytes = null;
		try {
			decryptedTextBytes = cipher.doFinal(encryptedTextBytes);
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}

		return new String(decryptedTextBytes);
	}

	public static String generateSalt() {
		int saltlength = keySize / 8;
		SecureRandom random = new SecureRandom();
		byte bytes[] = new byte[saltlength];
		random.nextBytes(bytes);
		return new String(bytes);
	}

}
