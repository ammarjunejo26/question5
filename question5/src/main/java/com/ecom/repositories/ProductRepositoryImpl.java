package com.ecom.repositories;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ecom.entities.Product;

public class ProductRepositoryImpl extends GenericRepositoryAbstract<Product> implements IProductRepository {
    
	private static Logger log = LoggerFactory.getLogger(ProductRepositoryImpl.class);

	  public ProductRepositoryImpl() {
	    super();
	    this.setClassToBePassed(Product.class);
	  }
      
      /*
	public List<Product> getProducts(){

		try {

			Session session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();

			List<Product> productList = new ArrayList<Product>();
//			productList = session.createCriteria(Product.class).list();
			log.info("Producst query");

		    Query query = session.createQuery("From Product");
		    productList = (List<Product>) query.list();
//		    Query query = session.createQuery("From Product where name = :name");
//            query.setString("username", "table");

//            Product user = (Product)query.uniqueResult();

		    
			for(Product pro : productList){
				log.info("Producst data");
				log.info(pro.getName());
			}

			if(productList.isEmpty()){
				System.out.println("products not found");

			}
			session.getTransaction().commit();
			session.close();

			return productList;
		} catch (Exception e) {
			System.out.println("products error");
			e.printStackTrace();
			return null;
		}		
	}*/
	
			 /*

	public Product createProduct(Product product){

		try {

			Session session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.save(product);
			session.getTransaction().commit();
			session.close();

			return product;
		} catch (Exception e) {
			System.out.println("products error");
			e.printStackTrace();
			return null;
		}		
	}*/
}
