package com.ecom.repositories;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.query.Query;

import com.ecom.config.HibernateUtil;
import com.ecom.entities.User;

public class UserRepositoryImpl extends GenericRepositoryAbstract<User> implements IUserRepository {

	public UserRepositoryImpl() {
		super();
		this.setClassToBePassed(User.class);
	}

	public User validateUser(String username , String password){

		try {
			Session session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			System.out.println("validate user ");
			System.out.println("user: " +username);
			System.out.println("password "+ password);

			//		    Query query = session.createQuery("From User where username = :username and password= :password");
			//            query.setString("username", username);
			//            query.setString("password", password);
			//
			//		    User user = (User)query.uniqueResult();
			//
			CriteriaBuilder builder = session.getCriteriaBuilder();
			CriteriaQuery<User> query = builder.createQuery(User.class);
			Root<User> root = query.from(User.class);
			query.select(root);
			Query<User> q=session.createQuery(query);
			User user = (User)q.uniqueResult();


			session.getTransaction().commit();
			session.close();

			if(user!=null){
				return user;
			}
			else{
				return null;
			}
		} catch (Exception e) {
			System.out.println("username not found");
			e.printStackTrace();
			return null;
		}		
	}

}
