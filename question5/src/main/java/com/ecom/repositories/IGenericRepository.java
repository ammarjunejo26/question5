package com.ecom.repositories;

import java.util.List;

public interface IGenericRepository <T>{
	
	public T findById(final Integer id);

	public List<T> findAll();
	
	public T create(final T entity);

	public T update(final T entity);
	
	public void delete(final T entity);	
	
}
