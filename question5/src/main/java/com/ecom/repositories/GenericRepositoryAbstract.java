package com.ecom.repositories;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ecom.config.HibernateUtil;

public class GenericRepositoryAbstract<T> implements IGenericRepository<T>
{
	private Class<T> classToBePassed;
	private Session session = HibernateUtil.getSessionFactory().openSession();
	private static Logger log = LoggerFactory.getLogger(GenericRepositoryAbstract.class);

	public final void setClassToBePassed(final Class<T> clazzToSet)
	{
		this.classToBePassed = clazzToSet;
	}

	public T findById(final Integer id)
	{
		T entity = null;
		try{
			session.beginTransaction();
			//			entity = session.load(this.classToBePassed, id);
			session.close();

			return entity;
		}
		catch (Exception e){
			return null;
		}
	}

	public List<T> findAll()
	{
		List<T> entityList = null;
		try{
			CriteriaQuery<T> cq = session.getCriteriaBuilder().createQuery(classToBePassed);
			cq.from(classToBePassed);
			entityList = session.createQuery(cq).getResultList();
			session.close();
			return entityList;
		}
		catch (Exception e){
			return null;
		}
	}

	public T create(final T entity)
	{
		try
		{
			session.beginTransaction();
			session.save(entity);
			session.getTransaction().commit();
			session.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return entity;
	}

	public T update(final T entity)
	{
		log.info("CAME HERE UPDATE");

		session.beginTransaction();
		session.update(entity);
		session.getTransaction().commit();
		session.close();

		return entity;
	}

	public void delete(final T entity)
	{
		log.info("CAME HERE delete");
		try
		{
			session.beginTransaction();
			session.remove(entity);
			session.getTransaction().commit();
			session.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
