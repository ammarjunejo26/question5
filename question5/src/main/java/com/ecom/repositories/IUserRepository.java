package com.ecom.repositories;

import com.ecom.entities.User;

public interface IUserRepository extends IGenericRepository<User>{
	
	public User validateUser(String username , String Password);

}
