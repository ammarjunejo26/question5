package com.ecom.controller;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named("testingController")
@RequestScoped
public class TestingController {
    private static Logger log = LoggerFactory.getLogger(TestingController.class);

	public String refresh(String xyz) {
		
		log.info("REFRESH GRID " + xyz);
		
		return null;
		
	}
	

}
