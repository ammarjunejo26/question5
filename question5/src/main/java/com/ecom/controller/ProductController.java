package com.ecom.controller;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.ShortBufferException;
import javax.faces.application.FacesMessage;
import javax.faces.application.ViewExpiredException;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ecom.entities.Product;
import com.ecom.services.IProductService;

@Named("productController")
@ViewScoped
public class ProductController implements Serializable {

	private static final long serialVersionUID = 1L;

	private static Logger log = LoggerFactory.getLogger(ProductController.class);

	@Inject
	IProductService productService; 

	private List<Product> products;

	private Product selectedProduct=null;
	private boolean showEditPanel =false;

	private String productname = null;
	//	private BigDecimal price = null;
	private String price = null;

	@PostConstruct
	public void init(){
		try {
			products = productService.getProducts();
			showEditPanel = false;
			selectedProduct = new Product();
		}catch (InvalidKeyException e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("InvalidKeyException"));
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("NoSuchAlgorithmException"));
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("NoSuchPaddingException"));
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("IllegalBlockSizeException"));
			e.printStackTrace();
		} catch (BadPaddingException e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("BadPaddingException"));
			e.printStackTrace();
		} catch (ShortBufferException e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("ShortBufferException"));
			e.printStackTrace();
		} catch (NoSuchProviderException e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("NoSuchProviderException"));
			e.printStackTrace();
		}
		catch (ViewExpiredException e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("The session was expired. Please try again"));
			e.printStackTrace();
		}
		catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Server error"));
			e.printStackTrace();
		}
	}

	public String add()  {

		Product product = new Product();
		product.setName(getProductname());
		product.setPrice(getPrice());

		log.info("ADD PRODUCT");

		try {
			product = productService.createProduct(product);
			productname = null;
			price = null;

			return "successful";
		} catch (InvalidKeyException e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("InvalidKeyException"));
			e.printStackTrace();
			return "failed";

		} catch (NoSuchAlgorithmException e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("NoSuchAlgorithmException"));
			e.printStackTrace();
			return "failed";

		} catch (NoSuchPaddingException e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("NoSuchPaddingException"));
			e.printStackTrace();
			return "failed";

		} catch (IllegalBlockSizeException e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("IllegalBlockSizeException"));
			e.printStackTrace();
			return "failed";

		} catch (BadPaddingException e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("BadPaddingException"));
			e.printStackTrace();
			return "failed";

		} catch (UnsupportedEncodingException e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("UnsupportedEncodingException"));
			e.printStackTrace();			
			return "failed";

		} catch (ShortBufferException e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("ShortBufferException"));
			e.printStackTrace();
			return "failed";

		} catch (NoSuchProviderException e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("NoSuchProviderException"));
			e.printStackTrace();
			return "failed";

		}
		catch (ViewExpiredException e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("The session was expired. Please try again"));
			e.printStackTrace();
			return "failed";

		}
		catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Server error"));
			e.printStackTrace();
			return "failed";
		}
	}

	public String refresh() {

		log.info("REFRESH GRID ");
		try{
			products = productService.getProducts();

		}
		catch (ViewExpiredException e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("The session was expired. Please try again"));
			e.printStackTrace();
			return "failed";

		}
		catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Server error"));
			e.printStackTrace();
			return "failed";

		}
		return null;

	}

	public String delete(Product prod) {
		log.info("prod.getName " + prod.getName());

		try{
			productService.deleteProduct(prod);
		}
		catch (ViewExpiredException e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("The session was expired. Please try again"));
			e.printStackTrace();
			return "failed";

		}
		catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Server error"));
			e.printStackTrace();
			return "failed";

		}
		return null;
	}

	public void select(Product product){

		log.info("selectedProduct SET " + product.getName());
		selectedProduct = product;

		showEditPanel = true;
	}


	public String update(){

		Product product = new Product();
		product.setId(selectedProduct.getId());
		product.setName(selectedProduct.getName());
		product.setPrice(selectedProduct.getPrice());
		log.info("UPDATE GRID " + product.getName());
		try{
			productService.editProduct(product);
			selectedProduct = new Product();
			showEditPanel = false;
		}
		catch (InvalidKeyException e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("InvalidKeyException"));
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("NoSuchAlgorithmException"));
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("NoSuchPaddingException"));
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("IllegalBlockSizeException"));
			e.printStackTrace();
		} catch (BadPaddingException e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("BadPaddingException"));
			e.printStackTrace();
		} catch (ShortBufferException e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("ShortBufferException"));
			e.printStackTrace();
		} catch (ViewExpiredException e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("The session was expired. Please try again"));
			e.printStackTrace();
			return "failed";
		}
		catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Server error"));
			e.printStackTrace();
			return "failed";
		}
		return null;	
	}

	public String getProductname() {
		return productname;
	}
	public void setProductname(String productname) {
		this.productname = productname;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public List<Product> getProducts() {
		return products;
	}
	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public Product getSelectedProduct() {
		return selectedProduct;
	}

	public void setSelectedProduct(Product selectedProduct) {
		this.selectedProduct = selectedProduct;
	}

	public boolean isShowEditPanel() {
		return showEditPanel;
	}

	public void setShowEditPanel(boolean showEditPanel) {
		this.showEditPanel = showEditPanel;
	}


}
