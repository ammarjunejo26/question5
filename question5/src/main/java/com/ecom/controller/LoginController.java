package com.ecom.controller;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ecom.config.SessionUtils;
import com.ecom.entities.User;
import com.ecom.services.IUserService;

@Named
@SessionScoped
public class LoginController implements Serializable{

	private static final long serialVersionUID = 1L;

	private static Logger log = LoggerFactory.getLogger(LoginController.class);

	private User currentUser;

	@Inject
	IUserService userService;
	
	public User getCurrentUser() {
		return currentUser;
	}
	public void setCurrentUser(User currentUser) {
		this.currentUser = currentUser;
	}

	private String username;
	private String password;

	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public String submit() {
		System.out.println(username + " <== : ==> "  + password);
		
		User user = new User();
		try {
			System.out.println("username: " +username);
			System.out.println("password: " +password);
			user = userService.login(username,password);

			if (user!=null) {
				HttpSession session = SessionUtils.getSession();
				session.setAttribute("username", user);
				currentUser=user;
			}
			else{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Please enter a valid username/password"));	
				return "failure";
			}
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "failure";
		}
		return "success";
	}

	public String logout() {
		log.info("logout call");

		HttpSession session = SessionUtils.getSession();
		session.invalidate();
		return "logout";
	}
}
